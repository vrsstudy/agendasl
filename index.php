<?php  
/* esse bloco de código em php verifica se existe a sessão, pois o usuário pode 
simplesmente não fazer o login e digitar na barra de endereço do seu navegador 
o caminho para a página principal do site (sistema), burlando assim a obrigação 
de fazer um login, com isso se ele não estiver feito o login não será 
criado a session, então ao verificar que a session não existe a página 
redireciona o mesmo para a index.php. */
session_start();
if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
{
  unset($_SESSION['login']);
  unset($_SESSION['senha']);
  header('location:login.php');
}

$logado = $_SESSION['login'];
?>
<!DOCTYPE html>
<html lang="PT-Br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">

  <title>AgendaSL</title>
  <link rel="icon" href="imagens\favicon-sl.ico" type="image/x-icon" />

  <!-- Bootstrap core CSS -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- CSS PERSONALIZADO -->
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="scss\style.scss">
  <link rel="stylesheet" href="fonts\fa\css\all.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>  
</head>
<?php
include "config.php";

?>

<body>

  <!-- Static navbar -->
  <nav class="navbar navbar-default navbar-fixed ">
    <div class="container">
      <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed navbar-light" data-toggle="collapse" data-target="#funcionakct" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" ><i class="fas fa-phone-alt"></i> AGENDASL</i></a>
      </div>
      <div id="funcionakct" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="index.php?page="><i class="fas fa-home"></i> Início</a></li>
         <?php

        if ($_SESSION['login'] == 'admin') {
         echo '<li><a href="index.php?page=form_contato"><i class="fas fa-plus"></i> Novo contato</a></li>';
        }
          ?>
          <li><a href="index.php?page=listar_contatos&contato="><i class="fas fa-check"></i> Listar</a></li>
          <li><a href="logout.php?sair=logout"><i class="fas fa-sign-out-alt"></i> Sair</a></li>
            
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <form class="navbar-form navbar-left" role="search" name="busca" action="index.php">
              <div class="form-group form-search">
              <li role="separator" class="divider"></li>
                <input type="hidden" name="page" value="listar_contatos" />          
                <input type="text" name="contato" class="form-control-search" placeholder="Procurando alguém?">
                <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
              </div>
          
            </form>
          </ul>
          
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      <?php 
      $page='';
      if( empty($_REQUEST['page'])){  
        ?>
        <div class="jumbotron">
          <h2><?php echo "Bem vindo(a) ".$logado ?> - AgendaSL</h2>
          <p>Aqui você cadastra os seus contatos e pode realizar buscas a qualquer momento e em qualquer lugar!</p>
        </div>
        <?php }else{
          $pagina = $_REQUEST['page'];
          include ($pagina.'.php');
        }
        ?>

      </div> <!-- /container -->

      <?php
      mysqli_close($con);

      ?>

      <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="bootstrap/js/bootstrap.js"></script>

      </body>
      </html>



