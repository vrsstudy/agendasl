
<!DOCTYPE html>
<html lang="PT-Br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">

  <title>AgendaSL</title>
  <link rel="icon" href="imagens\favicon-sl.ico" type="image/x-icon" />
  
  <!-- Bootstrap core CSS -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="fonts\fa\css\all.css">

  <!-- CSS LOGIN -->
  <link href="css/login.css" rel="stylesheet">

  
</head>
<?php
include "config.php";

?>

<body>




  <div class="container">
   
    <form class="form-signin" method="POST" action="dao/verifica_login.php">
    <div align="center">
      <img src="imagens/logo-saolucas-branca.svg" align="center" position="fixed">
    </div>
    <div class="login-box">
  <h1>Entrar</h1>
  <div class="textbox">
    <i class="fa fa-user"></i>
    <input type="text" name="login" placeholder="Usuário">
  </div>

  <div class="textbox">
    <i class="fa fa-lock"></i>
    <input type="password" name="senha" placeholder="Senha">
  </div>
          <button class="btn btn-lg btn-primary btn-block" name="entrar" type="submit">Login</button>
          
            <?php
                if(isset($_GET['erro'])){
                  echo '<div class="alert alert-info error">';
                  echo "Login ou senha inválidos!";
                 
                }
            ?>
          </div>
        </form>

      </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>
    </html>