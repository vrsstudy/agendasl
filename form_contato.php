<div class="row">

  <form class="form-horizontal" name="agenda" action="dao/cad_contato.php" method="post" >
    <div class="form-group">
      <label>Nome</label>
      <input type="text" class="form-control" name="nome" placeholder="Nome" autofocus required>
    </div>
    <div class="form-group">
      <label>E-mail</label>
      <input type="email" class="form-control" name="email" placeholder="exemplo@exemplo.com.br" required>
    </div>
    <div class="form-group">
      <label>Ramal 1</label>
      <input type="tel" class="form-control" name="ramal1" pattern="[0-9]{4}$" placeholder="1234">
    </div>
    <div class="form-group">
      <label>Ramal 2</label>
      <input type="tel" class="form-control" name="ramal2" pattern="[0-9]{4}$" placeholder="1234" required>
    </div> 
    <div class="form-group">
      <label>Unidade</label>
      <select class="form-control" name="unidade" type="text" required>
       <option>São Lucas - SEDE</option>
       <option>São Lucas - Campus II</option>
       <option>São Lucas - Ji-Paraná</option>
       <option>São Lucas - Caçapava</option>
       <option>Centro de Serviços Compartilhados - CSC</option>
</select>
    </div> 
    <button type="submit" class="btn btn-primary">Cadastrar</button>
    <button type="reset" class="btn btn-primary">Limpar</button>
  </form>

</div>